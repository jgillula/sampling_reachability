function result = sr_feasible(point, body, parameters)
tic;
result = false;
n = body.dims;
N = parameters.feasibility_params.N;
x0 = point';
% umax = parameters.system.umax;
U = parameters.system.U;
m = U.dims;
Fu_big = parameters.feasibility_params.Fu_big;
fu_big = parameters.feasibility_params.fu_big;

switch body.shape_type
  case 'polytope'
    F_big = [];
    f_big = [];
    k = 0;
    [F, f] = double(parameters.feasibility_params.K_down);
    [num_rows, num_cols] = size(F);
    F_big((end+1):(end+num_rows), (end+1):(end+n)) = F;
    f_big((end+1):(end+num_rows),1) = f;
%     F_big(((k*n)+1):((k+1)*n), ((k*n)+1):((k+1)*n))
%     F_big(((k*n)+1):((k+1)*n), ((k*n)+1):((k+1)*n)) = F;
%     f_big(((k*n)+1):((k+1)*n), 1) = f;
    
    gamma = zeros(N,1);
    for k=1:N
      gamma(k) = parameters.feasibility_params.gamma_k_term1(k)*norm(x0, inf); + parameters.feasibility_params.gamma_k_term2(k);
      K_down_k = parameters.feasibility_params.K_down-unitbox(n, gamma(k));
      [num_rows, num_cols] = size(F);
      F_big((end+1):(end+num_rows), (end+1):(end+n)) = F;
      f_big((end+1):(end+num_rows),1) = f;
    end
    
    tSetup = toc;
    tic;
    
    switch parameters.solver
      case 'cvx'
        cvx_begin
        cvx_quiet(parameters.cvx_quiet);
        variables ubar(N) xbar(n*(N+1));
        minimize( 0 );
        subject to
        ubar >= -parameters.system.umax;
        ubar <= parameters.system.umax;;
        xbar == parameters.feasibility_params.G*x0 + parameters.feasibility_params.H*ubar;
        F_big*xbar <= f_big; %polytope case
        cvx_end
        
      case 'yalmip'
        
        opts = sdpsettings;
        opts.verbose = 0;
        
        ubar   = sdpvar(m*N, 1);
        xbar   = sdpvar(n*(N+1),1);
        
        Constraints = [ Fu_big*ubar <= fu_big, ...
          xbar == parameters.feasibility_params.G*x0 + parameters.feasibility_params.H*ubar, ...
          F_big*xbar <= f_big ];
        
        sdp_ans = solvesdp(Constraints,0,opts);
        
      otherwise
        
        error('Solver can only be CVX or YALMIP.')
    end
  otherwise
    error('Shape %s has not yet been implemented for feasibility checking!', body.shape_type);
end

switch parameters.solver
  case 'cvx'
    if cvx_optval == 0
      result = true;
    end
    
  case 'yalmip'
    if sdp_ans.problem == 0
      result = true;
    end
end

tElapsed = toc;
if(parameters.verbose)
  fprintf('%2.2f (%2.2f)s...', tElapsed, tSetup);
end