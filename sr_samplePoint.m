function result = sr_samplePoint(shape)
  switch shape.shape_type
    case 'polytope'
      [F f] = double(shape.polytope);
      result = cprnd(1, F, f);
    otherwise
      error('Unknown shape type ''%s''', shape.shape_type);
  end