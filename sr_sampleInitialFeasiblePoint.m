function result = sr_sampleInitialFeasiblePoint(K, parameters)

if(parameters.verbose)
  fprintf('  Generating initial feasbile point...');
end
tic;


switch K.shape_type
  case 'polytope'
    [F, f] = double(K.polytope);
    [c,r] = chebycenter(F,f);
    result = c';
  otherwise
    error('Unknown shape ''%s''', K.shape_type);
end


while(~sr_feasible(result, K, parameters))
  result = sr_samplePoint(K);
  pause(0.001);
end

if(parameters.verbose)
  tElapsed = toc;
  fprintf('Done! (Took %2.2f sec)\n', tElapsed);
end