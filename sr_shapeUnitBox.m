function result = sr_shapeUnitBox(radius, dims, parameters)
  result.dims = dims;
  result.shape_type = 'polytope';
  result.polytope = unitbox(dims, radius);
  result.radius = radius;
  
  if(parameters.plot_2D)
    result.vertices = extreme(result.polytope);
  end
