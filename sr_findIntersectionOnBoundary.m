function result = sr_findIntersectionOnBoundary(K, origin, rays, parameters)

num_rays = size(rays,2);
dim = size(rays,1);
result = zeros(num_rays, dim);

if(parameters.verbose)
  fprintf('  Calculating boundary intersections...');
  tStart = tic;
end

switch K.shape_type
  case 'polytope'
    [F, f] = double(K.polytope);
    tic;
    for i = 1:num_rays
      pause(0.001);
      ray = rays(:,i);
      s = (f - F*origin')./(F*ray);
      result(i,:) = origin + ray'*min(s(find(s>=0)));
      pause(0);
      tElapsed = toc;
      if(tElapsed >= 1 && parameters.verbose)
        fprintf('%d...', i);
        tic;
      end
    end
  otherwise
    error('Boundary intersection for shape type %s not yet implemented!', K.shape_type);
end

if(parameters.verbose)
  tElapsed = toc(tStart);
  fprintf('Done! (Took %2.2f sec)\n', tElapsed);
end