function [K, parameters] = sr_plotApprox2DProjections(K, approx, figures, parameters)
if ~exist('figures', 'var')
  figures = [];
end
num_dims = size(approx,2);

if max(size(parameters.plot_2D)) > 1
  num_figures = size(parameters.plot_2D, 1);
  if length(figures) ~= num_figures
    figures = zeros(num_figures,1);
    for i=1:num_figures
      figures(i) = figure;
      pause(0.25);
    end
  end
else
  if size(figures) ~= [num_dims, num_dims]
    figures = zeros(num_dims, num_dims);
    for i=1:num_dims
      for j=(i+1):num_dims
        figures(i,j) = figure;
        pause(0.25);
      end
    end
  end
end

if ~exist('parameters', 'var')
  parameters = [];
end

default_parameters.K_polytope_plot_options = struct('color', [1 0 0]);
default_parameters.approx_plot_options = struct('linewidth', 1, 'color', 'g');
default_parameters.plot_K = true;
default_parameters.plot_K_down = true;
default_parameters.K_down_polytope_plot_options = struct('color', [0.5 0 0]);
default_parameters.plot_last_vertex = true;
default_parameters.axis_label_options = struct('FontSize', 16);
default_parameters.clear_before_plotting = true;

parameters_fieldnames = fieldnames(default_parameters);
num_fields = size(parameters_fieldnames,1);
for i = 1:num_fields
  field = parameters_fieldnames{i,1};
  if(not(isfield(parameters, field)))
    parameters.(field) = default_parameters.(field);
  end
end

if(~isfield(K, 'vertices') && strcmp(K.shape_type, 'polytope'))
  K.vertices = extreme(K.polytope);
end
if(~isfield(parameters.feasibility_params, 'K_down_vertices') && strcmp(K.shape_type, 'polytope'))
  parameters.feasibility_params.K_down_vertices = extreme(parameters.feasibility_params.K_down);
end

if max(size(parameters.plot_2D)) > 1
  for k=1:num_figures
    [K, parameters] = sr_plot2DProjection(figures(k), parameters.plot_2D(k,1), parameters.plot_2D(k,2), ...
      parameters, K, approx);
  end
else
  for i=1:num_dims
    for j=(i+1):num_dims
      [K, parameters] = sr_plot2DProjection(figures(i,j), i, j, ...
        parameters, K, approx);
    end
  end
end

parameters.figures = figures;