function [K, parameters] = sr_plot2DProjection(figure, i, j, parameters, K, approx)
set(0, 'CurrentFigure', figure);
if(parameters.clear_before_plotting)
  clf(figure);
end
hold on;
if(parameters.plot_K)
  if ~isfield(K, 'proj')
    K.proj = {};
  end
  if size(K.proj,1) < i || size(K.proj,2) < j
    K.proj{i,j} = [];
  end
  switch K.shape_type
    case 'polytope'
      if(isempty(K.proj{i,j}))
        K.proj{i,j} = polytope(unique(K.vertices(:, [i j]), 'rows'));
      end
      plot(K.proj{i,j}, parameters.K_polytope_plot_options);
      axis equal;
    otherwise
      error('Plotting for shape type %s has not yet been implemented!', K.shape_type);
  end
end

if(parameters.plot_K_down)
  if ~isfield(parameters.feasibility_params, 'K_down_proj')
    parameters.feasibility_params.K_down_proj = {};
  end
  if size(parameters.feasibility_params.K_down_proj,1) < i || size(parameters.feasibility_params.K_down_proj,2) < j
    parameters.feasibility_params.K_down_proj{i,j} = [];
  end
  switch K.shape_type
    case 'polytope'
      if(isempty(parameters.feasibility_params.K_down_proj{i,j}))
        parameters.feasibility_params.K_down_proj{i,j} = polytope(unique(parameters.feasibility_params.K_down_vertices(:, [i j]), 'rows'));
      end
      plot(parameters.feasibility_params.K_down_proj{i,j}, parameters.K_down_polytope_plot_options);
      axis equal;
    otherwise
      error('Plotting for shape type %s has not yet been implemented!', K.shape_type);
  end
  
end

proj = polytope(approx(:, [i j]));
plot(proj, parameters.approx_plot_options);
if(parameters.plot_last_vertex)
  plot(approx(end,i), approx(end, j), 'b*');
end
if(isfield(parameters.system, 'state_names'))
  foo = xlabel(sprintf('$%s$', parameters.system.state_names{i}), 'Interpreter', 'latex');
  set(foo, parameters.axis_label_options);
  foo = ylabel(sprintf('$%s$', parameters.system.state_names{j}), 'Interpreter', 'latex');
  set(foo, parameters.axis_label_options);
else  
  xlabel(sprintf('x_%d', i));%, parameters.axis_label_options);
  ylabel(sprintf('x_%d', j));%, parameters.axis_label_options);
end
axis equal;
grid on;
set(gca,'Layer','top');
drawnow;
hold off;
