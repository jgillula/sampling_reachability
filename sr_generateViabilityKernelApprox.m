function result = sr_generateViabilityKernelApprox(K, parameters)
  if(parameters.verbose)
    fprintf('Beginning viability kernel approximation!\n');
  end
  pause(0.001);
  
  parameters.feasibility_params = sr_setupFeasibilityParams(K, parameters.system, parameters);

  default_parameters.K_0 = sr_sampleInitialFeasiblePoint(K, parameters);

  parameters_fieldnames = fieldnames(default_parameters);
  num_fields = size(parameters_fieldnames,1);
  for i = 1:num_fields
    field = parameters_fieldnames{i,1};
    if(not(isfield(parameters, field)))
      parameters.(field) = default_parameters.(field);
    end
  end
  
  result.initial_point = parameters.K_0;
  result.parameters = parameters;
  result.K = K;     
  
  tStart = tic;
  result.approx = sr_extendPolytopicApprox(parameters.K_0, K, parameters.N, parameters);
  result.total_time = toc(tStart);
