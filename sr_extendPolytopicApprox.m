function result = sr_extendPolytopicApprox(K_0, K, N_random, parameters)
  
  if ~isempty(parameters.warm_start)
    N_start = size(parameters.warm_start,1);
    result = [parameters.warm_start; zeros(N_random, K.dims)];
    r = [sr_sampleSphereUniform(K.dims, N_random)];
  else    
    N_start = 0;
    N_fixed = K.dims*2;
    result = zeros(N_random+2*K.dims, K.dims);
    r = [sr_sampleAxes(K.dims) sr_sampleSphereUniform(K.dims, N_random)];
  end
  b = sr_findIntersectionOnBoundary(K, K_0, r, parameters);
  
  for i = 1:size(b,1);
    if(parameters.verbose)
      fprintf('  Finding point %d/%d...', i, size(b,1));
    end
    tStart = tic;
    if sr_feasible(b(i,:), K, parameters)
      result(i+N_start, :) = b(i, :);
    else
      result(i+N_start, :) = sr_bisectionFeasibilitySearch(K_0, b(i,:), K, parameters.bisection_max_depth, parameters);
    end
    tEnd = toc(tStart);
    if(parameters.verbose)
      fprintf('done! (Took %2.2f sec)\n', tEnd);
    end
    if parameters.plot_2D
      [K, parameters] = sr_plotApprox2DProjections(K, result(1:(i+N_start), :), parameters.figures, parameters);
      pause(0.01);
    end
  end