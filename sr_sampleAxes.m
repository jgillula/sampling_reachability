function result = sr_sampleAxes(num_dims)
  result = zeros(num_dims, 2*num_dims);
  for i=1:num_dims
    foo = zeros(num_dims,1);
    foo(i) = 1;
    result(:,2*(i-1)+1) = foo;
    result(:, 2*i) = -foo;
  end