function [ g, ctrl, inev, unsafe ] = DoubleIntegratorSampled_LS(delta, max_steps, accuracy)
% THIS IS IAN'S LS CODE FOR THE SAMPLED DATA VIABILITY KERNEL. I MODIFIED
% IT TO WORK FOR THE DOUBLE INTEGRATOR. SHAHAB MAY 6TH, 2013

% varyingDoubleIntegrator: time-sampled reachability for modified double integrator
%
%   [ g, ctrl, inev, unsafe ] = varyingDoubleIntegratorSampled(delta, max_steps, accuracy)
%  
% This is the calculation of the inevitably unsafe set, the control donut,
% the free set and the corresponding control laws for the state-varying
% double integrator
%
%     \dot x_1 = x_2
%     \dot x_2 = cos(2 * pi * x_1) * u
%        with input |u| \leq U and U is some constant.
%
% There are many options in the function which can only be modifed by
% changing the file.
%
% Input Parameters:
%
%   delta: Positive scalar.  Time sample period.  Default = 0.3.
%
%   max_steps: Positive integer.  Number of steps in the horizon.
%   Optional.  Default = 50 (which is large, but appears to be necessary
%   for convergence).
%
%   accuracy: String.  Order of accuracy of the numerical integrators.
%   Options are 'medium' (ENO2, RK2) or 'veryHigh' (WENO5, RK3).  The
%   latter takes about four times as long.  Default = 'medium'.
%
% Output Parameters:
%
%   g: Grid structure on which data was computed.
%
%   ctrl: Array.  Implicit surface function for the control donut set
%   (including the inevitably unsafe set and the unsafe target set).
%
%   inev: Array.  Implicit surface function for the inevitably unsafe set
%   (including the unsafe target set).
%
%   unsafe: Array.  Implicit surface function for the target unsafe set
%   (the initial conditions).
%
% Although the output parameters ctrl and inev are implicit surface
% functions for the combination of those sets and the sets they contain,
% the implicit surface functions for those sets only can be constructed
% using the intersection (eg: max) and complement (eg: multiply by -1)
% operators.

% Copyright 2011 Ian M. Mitchell (mitchell@cs.ubc.ca).
% This software is used, copied and distributed under the licensing 
%   agreement contained in the file LICENSE in the top directory of 
%   the distribution.
%
% Created by Ian Mitchell, 2009/11/21

  %---------------------------------------------------------------------------
  % You will see many executable lines that are commented out.
  %   These are included to show some of the options available; modify
  %   the commenting to modify the behavior.

  % Tell m-lint not to flag unreachable code in this file: There are a
  % bunch of problem / solver parameters which are set at the beginning of
  % this file.  For any given value of these parameters, some code may be
  % unreachable, but we want to keep it because we may change these
  % parameters later.
  %#ok<*UNRCH>
  
  start_time_overall = cputime;

  %---------------------------------------------------------------------------
  %% Make sure we can see the kernel m-files.
  %run('../addPathToKernel');

  %---------------------------------------------------------------------------
  %% Optional input arguments.
  if(nargin < 1)
    delta = 0.05;
  end
  if(nargin < 2)
    max_steps = 20;
  end
  if(nargin < 3)
    accuracy = 'medium';
  end
  
  %---------------------------------------------------------------------------
  %% PDE solver parameters.

  % How many intermediate plots to produce (maximum)?  Since each plot must
  % fall on a sample time boundary, the actual number will depend on t_max
  % and delta.
  plot_steps = 9;

  % How many sample times between plots?
  plot_samples = ceil(max_steps / (plot_steps - 1));

  % Plot after each timestep?  Overrides t_plot.
  single_step = false;

  % Pause after each plot?
  pause_after_plot = false;

  % Delete previous plot before showing next?
  delete_last_plot = false;

  % Plot in separate subplots?  Set delete_last_plot = false in this case.
  use_subplots = 0;

  % Parameters for visualizeLevelSets.
  level = 0;
  display_type = 'contour';

  % What order of accuracy to use for the computation's spatial derivatives
  % and temporal integration?
  switch(accuracy)
    case 'veryHigh'
      derivFunc = @upwindFirstWENO5;
      integrator_func = @odeCFL3;
    case 'medium'
      derivFunc = @upwindFirstENO2;
      integrator_func = @odeCFL2;
    otherwise
      error('Unknown accuracy: %s', accuracy);
  end

  % Some integration parameters apply to any integrator.
  integrator_options = odeCFLset('factorCFL', 0.9, 'stats', 'on');
  if(single_step)
    integrator_options = odeCFLset(integrator_options, 'singleStep', 'on');
  end

  %---------------------------------------------------------------------------
  %% Optional and problem specific parameters.

  % Maximum magnitude of the input.
  input_bound = 0.15;

  % What inputs should we sample?
  num_input_samples = 7;
  input_samples = linspace(-input_bound, +input_bound, num_input_samples);

  % Compute with both positive and negative velocities, or only positive?
  negative_velocities = 1;

  % Should we only allow the reach set to grow?
  restrict_update = 1;

  % How to implement the dynamics in the HJ PDE for computing the
  % inevitable collision set.  Note that we will always use Lax-Friedrichs
  % for the control donut.
  dynamics_term = 'convection';
  % Could also implement as Lax-Freidrichs...

  %---------------------------------------------------------------------------
  %% Create the grid.  
  g.dim = 2;
  
  g.min = [ -0.7; -0.7 ];
  g.max = [ +0.7; +0.7 ];
  g.dx = 0.05;
  g.bdry = @addGhostExtrapolate;
  g = processGrid(g);

  %---------------------------------------------------------------------------
  %% Create initial conditions.
  data = shapeRectangleByCorners(g, [ -0.5; -.5 ], [ +.5, +.5 ]);

  % Then take the complement, since the outside of the set is the actual
  % target for the backwards reachability.
  data = -data;
  % This array is the target (unsafe) set.
  unsafe = data;

  %---------------------------------------------------------------------------
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %---------------------------------------------------------------------------
  %% Set up PDE terms that implement the dynamics.

  % First the term for approximating the inevitably unsafe set.
  switch(dynamics_term)
    case 'convection'
      % Since we are sampling the inputs and there are no disturbances, we
      % can compute the purely convectional velocity field for each input
      % sample.  Remember to reverse the dynamics since we solve the PDE
      % backward in time.
      scheme_func_inev = @termConvection;
      scheme_data_inev.grid = g;
      scheme_data_inev.derivFunc = derivFunc;

      velocity_samples = cell(num_input_samples, 1);
      for i_input_samples = 1 : num_input_samples
        velocity_samples{i_input_samples} = { -g.xs{2}; -(input_samples(i_input_samples)) };
      end

    %case 'LaxFriedrichs'
      % A Lax-Friedrichs implementation would be more representative of what
      % would be used for more complicated dynamics, although it is likely to
      % be more dissipative and hence less accurate.

    otherwise
      error('Unknown dynamics_term: %s', dynamics_term);
  end
  
  % Now a term for approximating the control donut.  Because this term
  % treats the control input as an optimal disturbance, we will solve using
  % a Lax-Friedrichs scheme.
  scheme_func_ctrl = @termLaxFriedrichs;
  scheme_data_ctrl.derivFunc = derivFunc;
  scheme_data_ctrl.hamFunc = @hamFunc;
  scheme_data_ctrl.partialFunc = @partialFunc;
  scheme_data_ctrl.dissFunc = @artificialDissipationGLF;
  scheme_data_ctrl.grid = g;
  scheme_data_ctrl.input_bound = input_bound;

  %---------------------------------------------------------------------------
  %% Restrict the Hamiltonian so that reachable set only grows.
  % Whatever scheme handles the dynamics must already be completely set up.
  if(restrict_update)
    innerFunc = scheme_func_inev;
    innerData = scheme_data_inev;
    clear scheme_func_inev scheme_data_inev;

    % Wrap the true Hamiltonian inside the term approximation restriction routine.
    scheme_func_inev = @termRestrictUpdate;
    scheme_data_inev.innerFunc = innerFunc;
    scheme_data_inev.innerData = innerData;
    scheme_data_inev.positive = 0;

    innerFunc = scheme_func_ctrl;
    innerData = scheme_data_ctrl;
    clear scheme_func_ctrl scheme_data_ctrl;

    % Wrap the true Hamiltonian inside the term approximation restriction routine.
    scheme_func_ctrl = @termRestrictUpdate;
    scheme_data_ctrl.innerFunc = innerFunc;
    scheme_data_ctrl.innerData = innerData;
    scheme_data_ctrl.positive = 0;
  end

  %---------------------------------------------------------------------------
  %% Initialize Display
  f = figure; %#ok<NASGU>

  % Set up subplot parameters if necessary.
  if(use_subplots)
    rows = ceil(sqrt(plot_steps));
    cols = ceil(plot_steps / rows);
    plot_num = 1;
    subplot(rows, cols, plot_num);
  end

  h = visualizeLevelSet(g, data, display_type, level, 'target unsafe set');
  set(h, 'LineStyle', '--', 'LineWidth', 1, 'LineColor', 'blue');
  xlabel('position');  ylabel('velocity');

  hold on;
  axis(g.axis);
  daspect([ 1 1 1 ]);

  fprintf('Initialization time %g\n', cputime - start_time_overall);
  
  %---------------------------------------------------------------------------
  %% Prepare for the main loops for computing the inevitable collision set.
  sample_now = 0;
  
  fprintf('\nStarting computation of inevitably unsafe set.\n');
  start_time_inev = cputime;
  
  %---------------------------------------------------------------------------
  %% Loop over time samples.
  while(sample_now < max_steps)

    for i = 1 : plot_samples
    
      % Initial conditions for this time step (reshaped into the
      % form appropriate for the ODE solver call).
      y_init = data(:);
      y_end = [];

      % How far to step?
      t_span = [ 0, delta ];

      start_time_sample = cputime;
      
      %-----------------------------------------------------------------------
      %% Loop over input samples
      for i_input_samples = 1 : num_input_samples

        % Flow field for this input sample.
        if restrict_update
          % We need to modify the velocity field on the inner scheme_data_inev
          % structure.  This is where object orientation would help.
          scheme_data_inev.innerData.velocity = velocity_samples{i_input_samples};
        else
          % There is only one scheme_data_inev structure.
          scheme_data_inev.velocity = velocity_samples{i_input_samples}; %#ok<UNRCH>
        end

        % Take a timestep. Note that the different input samples may have
        % slightly different end times, and we are not properly adjusting
        % for that fact when we ignore the first return parameter
        [ ~, y ] = feval(integrator_func, scheme_func_inev, t_span, y_init, integrator_options, scheme_data_inev);
        
        % Outcome is the union of safe sets over all samples.
        if isempty(y_end)
          y_end = y;
        else
          y_end = max(y_end, y);
        end
      
      end

      % Get back the correctly shaped data array
      data = reshape(y_end, g.shape);

      fprintf('  Finished time sample step %d in %g seconds.\n\n', sample_now, cputime - start_time_sample);
      
      % Increment the number of time samples we have taken, and if
      % necessary stop looping.
      sample_now = sample_now + 1;
      if(sample_now >= max_steps)
        break;
      end
    end
    
    if(pause_after_plot)
      % Wait for last plot to be digested.
      pause;
    end

    % Delete last visualization if necessary.
    if(delete_last_plot)
      delete(h);
    end

    % Move to next subplot if necessary.
    if(use_subplots)
      plot_num = plot_num + 1;
      subplot(rows, cols, plot_num);
    end

    % Create new visualization.
    h = visualizeLevelSet(g, data, display_type, level, [ 'steps = ' num2str(sample_now) ]);
    set(h, 'LineStyle', '-', 'LineWidth', 1, 'LineColor', 'black');
    daspect([ 1 1 1 ]);

  end

  %---------------------------------------------------------------------------
  %% Finished computing the inevitable collision region.
  inev = data;
  
  fprintf('\nInevitable collision computation time %g seconds\n', cputime - start_time_inev);

  %---------------------------------------------------------------------------
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %---------------------------------------------------------------------------
  %% Compute the control donut.
  fprintf('\nStarting computation of control donut set.\n');
  start_time_ctrl = cputime;
  
  % Take a single sample timestep treating the input as a disturbance,
  % starting from the inevitably unsafe set.
  
  % Reshape data array into column vector for ode solver call.
  y0 = inev(:);

  % How far to step?
  t_span = [ 0, delta ];

  % Take a timestep.
  [ ~, y ] = feval(integrator_func, scheme_func_ctrl, t_span, y0, integrator_options, scheme_data_ctrl);

  % Get back the correctly shaped data array
  ctrl = reshape(y, g.shape);

  fprintf('\nControl donut computation time %g seconds\n', cputime - start_time_ctrl);

  %---------------------------------------------------------------------------
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %---------------------------------------------------------------------------
  %% Finished with everything.
  
  figure;
  h_unsafe = visualizeLevelSet(g, unsafe, display_type, level);
  set(h_unsafe, 'LineStyle', '-', 'LineWidth', 1, 'LineColor', 'r');  
  hold on;
  h_inev = visualizeLevelSet(g, inev, display_type, level);
  set(h_inev, 'LineStyle', '-', 'LineWidth', 2, 'LineColor', 'm');
  h_inev = visualizeLevelSet(g, ctrl, display_type, level);
  set(h_inev, 'LineStyle', ':', 'LineWidth', 2, 'LineColor', 'b');
  
  legend('K_0', 'K_{\infty}', 'K_{free}', 'location', 'best');
  title(sprintf('delta = %4.2f, steps = %d, |u| \\leq %4.2f, input samples = %d, accuracy %s', ...
                delta, max_steps, input_bound, num_input_samples, accuracy));
  daspect([ 1 1 1 ]);
      
  fprintf('\nTotal computation time %g seconds\n', cputime - start_time_overall);

end


%---------------------------------------------------------------------------
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%---------------------------------------------------------------------------
function ham_value = hamFunc(~, ~, deriv, scheme_data)
% hamFunc: analytic Hamiltonian.
%
%   ham_value = hamFunc(t, data, deriv, scheme_data)
%
% This function implements the hamFunc prototype for the spatially-varying
% double integrator.
%
% Input parameters:
%
%   t: Time at beginning of timestep.
%
%   data: Data array.
%
%   deriv: Cell vector of the costate (\grad \phi).
%
%   scheme_data: A structure (see below).
%
% Output parameters:
%
%   ham_value: The analytic hamiltonian.
%
% scheme_data is a structure containing data specific to this Hamiltonian
% For this function it contains the field(s):
%
%   .grid: Grid structure.
%
%   .input_bound: Positive scalar.  Bound on the input magnitude.
%
% Ian Mitchell 2011/11/21

  checkStructureFields(scheme_data, 'grid', 'input_bound');

  % The dynamics are pretty simple.  The input is treated as a disturbance,
  % so it seeks to maximize the Hamiltonian.  Remember to reverse the
  % dynamics (multiply by -1) since we solve the PDE backwards in time.
  ham_value = -(deriv{1} .* scheme_data.grid.xs{2} ...
                + deriv{2} .* sign(scheme_data.grid.xs{2}) * scheme_data.input_bound);
  
end % function hamFunc().


%---------------------------------------------------------------------------
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%---------------------------------------------------------------------------
function alpha = partialFunc(~, ~, ~, ~, scheme_data, dim)
% partialFunc: Hamiltonian partial function.
%
% alpha = partialFunc(t, data, derivMin, derivMax, schemeData, dim)
%
% This function implements the partialFunc prototype for the the
% spatially-varying double integrator.
%
% It calculates the extrema of the absolute value of the partials of the 
% analytic Hamiltonian with respect to the costate (gradient).
%
% Parameters:
%
%   t: Time at beginning of timestep.
%
%   data: Data array.
%
%   deriv_min: Cell vector of minimum values of the costate (\grad \phi).
%
%   deriv_max: Cell vector of maximum values of the costate (\grad \phi).
%
%   scheme_data: A structure (see below).
%
%   dim: Dimension in which the partial derivatives is taken.
%
% Output Parameters:
%
%   alpha: Maximum absolute value of the partial of the Hamiltonian with
%   respect to the costate in dimension dim for the specified range of
%   costate values (O&F equation 5.12). Note that alpha can (and should) be
%   evaluated separately at each node of the grid.
%
% scheme_data is a structure containing data specific to this Hamiltonian
% For this function it contains the field(s):
%
%   .grid: Grid structure.
%
%   .input_bound: Positive scalar.  Bound on the input magnitude.
%
% Ian Mitchell 2011/11/22

  checkStructureFields(scheme_data, 'grid', 'input_bound');

  %% Determine the alpha.
  switch dim
      
    case 1
      alpha = abs(scheme_data.grid.xs{2});
      
    case 2
      alpha = scheme_data.input_bound;

    otherwise
      error('partialFunc() does not support dimension: %d', dim);
  end
        
end % function partialFunc().