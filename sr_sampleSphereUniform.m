function z = sr_sampleSphereUniform(dims, num_points)
  x=randn(dims,num_points);
  z=zeros(dims,num_points);

  for k=1:num_points
    z(:,k)=x(:,k)/norm(x(:,k),2);
  end
  
  