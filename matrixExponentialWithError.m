function [M, R, min_p] = matrixExponentialWithError(A, p)
  if ~exist('p', 'var')
    p = 0;
  end
  min_p = max(1, ceil(norm(A,inf)-2));
  p = max(p, min_p);
  
  epsilon = norm(A,inf)/(p + 2);
  
  M = zeros(size(A));
  for i=0:p
    M = M + A^i / factorial(i);    
  end
  
  R = ones(size(A))*(norm(A,inf)^(p+1) / (factorial(p+1)*(1-epsilon)));