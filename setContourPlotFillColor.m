function setContourPlotFillColor(handle, color)
  set(handle, 'Fill', 'on');
  children = get(handle, 'Children');
  for i=1:length(children)
    if(strcmp(get(children(i), 'Type'), 'patch'))
      set(children(i), 'FaceColor', color);
    end
  end
