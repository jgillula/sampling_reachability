function result = runSamplingBasedReachabilityExample(parameters)
  if ~exist('parameters', 'var')
    parameters = [];
  end

  if(not(isfield(parameters, 'example')))
    parameters.example = 'double_integrator';
  end
  if(not(isfield(parameters, 'shape_type')))
    parameters.shape_type = 'polytope';
  end
  if(not(isfield(parameters, 'plot_2D')))
    parameters.plot_2D = false;
  end
  
  if(isfield(parameters, 'verbose'))
    if(parameters.verbose)
      fprintf('Setting up default parameters and processing parameters...');
    end
  end
  tStart = tic;
  
  switch parameters.shape_type
    case 'polytope'
      switch parameters.example
        case 'double_integrator'
          default_parameters.K = sr_shapeUnitBox(0.5, 2, parameters);          
          default_parameters.system = sr_sysDoubleIntegrator(0.05, 0.15);
          default_parameters.system.U = sr_shapeUnitBox(default_parameters.system.umax, 1, parameters);
        case 'integrator_chain'
          if(not(isfield(parameters, 'integrator_chain_size')))
            parameters.integrator_chain_size = 2;
          end
          default_parameters.K = sr_shapeUnitBox(0.5, parameters.integrator_chain_size, parameters);          
          default_parameters.system = sr_sysIntegratorChain(0.05, parameters.integrator_chain_size);
          default_parameters.system.U = sr_shapeUnitBox(0.15, 1, parameters);
        otherwise
          error('Unknown example system ''%s''', parameters.example);
      end
  end

  default_parameters.N = 10;
  default_parameters.bisection_epsilon = 0.01;
  default_parameters.bisection_max_depth = 3;
  %default_parameters.num_sub_time_intervals = 7;
  default_parameters.T = [1 0];
  default_parameters.cvx_quiet = true;
  default_parameters.figures = [];
  default_parameters.warm_start = [];
  default_parameters.verbose = true;
  default_parameters.solver = 'cvx';
  default_parameters.p = 100;

  
  parameters_fieldnames = fieldnames(default_parameters);
  num_fields = size(parameters_fieldnames,1);
  for i = 1:num_fields
    field = parameters_fieldnames{i,1};
    if(not(isfield(parameters, field)))
      parameters.(field) = default_parameters.(field);
    end
  end

  tElapsed = toc(tStart);
  if(parameters.verbose)
    fprintf('Done! (Took %2.2f sec)\n', tElapsed);
  end
    
  result = sr_generateViabilityKernelApprox(parameters.K, parameters);