function result = sr_setupFeasibilityParams(shape, system, parameters)

a = system.a;
b = system.b;
M_delta = system.M_delta;
B_delta = system.B_delta;
psi_delta = system.psi_delta;
zeta = system.zeta;
n = length(a); %system dimension
m = size(b,2); %number of inputs
U = system.U;
Ts = system.Ts;
T = parameters.T;
p = parameters.p;
N = length(0:Ts:T(1)); %prediction horizon

% In order to calculate \tilde{gamma}_k, we precalculate a few terms
% we will use repeatedly
% term1(i,j+1) = (i choose j)*inf_norm(M_delta ^ j)*(psi_delta ^ (i-j))
% Note that the stupid 'j+1' term in the indexing is because matlab
% can't index by 0
for i=1:N
  for j=0:(i-1)
    term1(i,j+1) = nchoosek(i,j)*norm(M_delta^j, inf)*psi_delta^(i-j);
  end
end
% term2 = inf_norm(sum from j=0 to zeta(A^j*delta^(j+1)/(j+1)!))
term2_temp = 0;
for j=1:zeta
  term2_temp = term2_temp + (a^j)*Ts^(j+1)/factorial(j+1);
end
term2 = norm(term2_temp, inf);

% The next optimization program calculates sup_u(B*u)
opts = sdpsettings;
opts.verbose = 0;

switch U.shape_type
  case 'polytope'
    [F, f] = double(U.polytope);
    max_val = 0;
    sup_u = [];
    u = sdpvar(U.dims,1);
    r = sdpvar(shape.dims,1);
    Constraints = [F*u <= f, r == b*u];
    for i=1:shape.dims
      for mult =[-1, 1]
        sdp_ans = solvesdp(Constraints, mult*r(i), opts);
        if(sdp_ans.problem == 0)
          if(abs(double(r(i))) > max_val)
            max_val = abs(double(r(i)));
            sup_u = double(u);
          end
        else
          error('Problem solving for sup norm(B*u, inf)!');
        end
      end
    end
  otherwise
    error('Unknown shape ''%s''', U.shape_type);
end




for k=1:N
  gamma_k_term1(k) = 0;
  for l=0:(k-1)
    gamma_k_term1(k) = gamma_k_term1(k) + term1(k, l+1);
  end
  gamma_k_term2(k) = 0;
  for i=0:(k-1)
    for l=0:(i-1)
      gamma_k_term2(k) = gamma_k_term2(k) + term1(i, l+1);
    end
    gamma_k_term2(k) = gamma_k_term2(k)*term2 + (norm(M_delta, inf)+psi_delta)^i*psi_delta*Ts/(zeta+2);
  end
  gamma_k_term2(k) = gamma_k_term2(k)*norm(b*sup_u, inf);
end


result.gamma_k_term1 = gamma_k_term1;
result.gamma_k_term2 = gamma_k_term2;

switch shape.shape_type
  case 'polytope'
    %Now we calculate $M\delta$
    [F,f] = double(shape.polytope);
    [Fu, fu] = double(U.polytope);
    
    opts = sdpsettings;
    opts.verbose = 0;
    
    u = sdpvar(U.dims, 1);
    x = sdpvar(n,1);
    
    Constraints = [ Fu*u <= fu, ...
      F*x <= f ];
    
    sdp_ans = solvesdp(Constraints,-norm(a*x+b*u, inf),opts);
    
    result.K_down = shape.polytope - unitbox(n, Ts*norm(a*double(x) + b*double(u), inf));
    
    if(~isfulldim(result.K_down))
      fprintf('\nresult.K_down ');
      display(result.K_down);
      error('K_down is not full dimensional -- details printed above');
    end
    
    
    
    result.Fu_big = kron(Fu, eye(N));
    result.fu_big = repmat(fu, N, 1);
    
    %     [F,f] = double(Kdown);
    
    %     result.F_big = kron(eye(N+1), F);
    %     result.f_big = repmat(f,N+1,1);
    
  otherwise
    error('Unknown shape type %s when setting up feasibility parameters!', shape.shape_type);
end

% a_c = a;
% b_c = b;
% sysd = c2d(ss(a_c,b_c,eye(1,n),0),Ts);
a = M_delta; %sysd.a;
b = B_delta; %sysd.b;

result.N = N;

%Prediction matrices
H = [zeros(n,m)];
for i = 0:N-1,
  H = [H; a^i*b];
end
for i = 1:N-1,
  H(:,(m*i+1):(m*(i+1))) = [zeros(n,m); H(1:end-n,(m*(i-1)+1):(m*i))];
end
result.H = H;

G = [];
for i = 0:N,
  G = [G; a^i];
end
result.G = G;