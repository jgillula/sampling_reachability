function result = sr_sysIntegratorChain(Ts, state_dim, zeta)
  if nargin < 3
    zeta = 4;
  end
  
  result.a = [[zeros(state_dim-1,1) eye(state_dim-1)];
          zeros(1,state_dim)];
  result.b = [zeros(state_dim-1,1);1];

  result.Ts = Ts;
%   result.umax = umax;
  
  result.zeta = zeta;
  result.M_delta = sr_approxMatrixExp(result.a, Ts, zeta);
  result.B_delta = quadv(@(lambda) sr_approxMatrixExp(result.a, lambda, zeta), 0, Ts)*result.b;
  
  epsilon = norm(result.a, inf)*Ts/(zeta+2);
  if(epsilon >= 1)
    error('Incorrectly chosen zeta!  Need to choose zeta so epsilon < 1!');
  end
  result.psi_delta = (norm(result.a, inf)*Ts)^(zeta+1)/factorial(zeta+1)*1/(1-epsilon);