function result = sr_bisectionFeasibilitySearch(a, b, K, depth, parameters)  
  c = a + (b-a) / 2;
  pause(0.001);
  if sr_feasible(c, K, parameters)
%     norm(c-b)
    if (depth == 0) || (norm(c-b) < parameters.bisection_epsilon)      
      result = c;
      if(parameters.verbose)
        fprintf('(Depth=%d)', parameters.bisection_max_depth - depth);
      end
    else
      result = sr_bisectionFeasibilitySearch(c, b, K, depth-1, parameters);
    end
  else
%     norm(c-a)
    if (depth == 0) || (norm(c-a) < parameters.bisection_epsilon)
      result = a;
      if(parameters.verbose)
        fprintf('(Depth=%d)', parameters.bisection_max_depth - depth);
      end
    else
      result = sr_bisectionFeasibilitySearch(a, c, K, depth-1, parameters);
    end
  end