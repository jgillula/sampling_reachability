function result = sr_approxMatrixExp(A, delta, zeta)
  result = zeros(size(A));
  for i=0:zeta
    result = result + (A*delta)^i/factorial(i);
  end