function result = sr_overestimateError(K, approx, parameters)

  if(~strcmp(K.shape_type, 'polytope'))
    error('Error estimation for shapes other than polytopes not yet implemented!');
  end

  if(parameters.verbose)
    fprintf('Overestimating error...\n  Creating MPT representation from vertices...');
  end
  tStart = tic;
  tic;
  mpt_approx = polytope(approx);
  [F, f] = double(mpt_approx);
  [K_K, K_k] = double(K.polytope);
  n = length(f);
  tElapsed = toc;
  if(parameters.verbose)
    fprintf('Done! Result has %d facets. (Took %2.2f sec)\n', n, tElapsed);
  end

  
%   fig = sr_plotApprox2DProjections(K, approx);
%   title(sprintf('%d vertices', size(approx,1)));
%   hold on;
  
  if(parameters.verbose)
    fprintf('  Calculating volume of approx...');
  end
  tic;
  [foo, result.approx_vol] = convhulln(approx);
  tElapsed = toc;
  if(parameters.verbose)
    fprintf('Done!  (Took %2.2f sec)\n', tElapsed);
  end
  
  result.error = 0;
  for i=1:n
    tic;
    if(parameters.verbose)
      fprintf('  Calculating volume of removing face %d of %d...', i, n);
    end
    F_temp = [F([1:(i-1), (i+1):n], :); K_K];
    f_temp = [f([1:(i-1), (i+1):n]); K_k];
    mpt_overestimate = polytope(F_temp, f_temp);
    overestimate_approx = extreme(mpt_overestimate);
%     sr_plotApprox2DProjections(K, overestimate_approx, fig, struct('approx_plot_options', struct('linewidth', 1, 'color', 'g', 'shade', 0.1)));
    [foo, vol] = convhulln(overestimate_approx);
    result.error = result.error + (vol - result.approx_vol);
    tElasped = toc;
    if(parameters.verbose)
      fprintf('Done!  (Took %2.2f sec)\n', tElapsed);
    end
  end
  
  tElasped = toc(tStart);
  if(parameters.verbose)
    fprintf('Done overestimating error!  (Total operation took %2.2f sec)\n', tElapsed);
  end
